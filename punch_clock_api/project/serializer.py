from marshmallow import fields, post_load, validate
from flask_marshmallow import Marshmallow
from project.model import User, ClockPunch, ClockPunchStatus, ConcludedDay

ma = Marshmallow()


def configure(app):
    ma.init_app(app)


class UserListSchema(ma.Schema):
    class Meta:
        model = User
    user_id = fields.Str(dump_only=True)
    cpf = fields.Str(required=False)
    fullname = fields.Str(required=False)
    email = fields.Str(required=False)

    @post_load
    def make_middleman(self, data, **kwargs):
        return User(**data)

class LoginSchema(UserListSchema):
    password = fields.Str(required=True)
    
class UserInsertUpdateSchema(LoginSchema):
    user_id = fields.Str(required=False)
    fullname = fields.Str(required=True)
    email = fields.Str(required=True)

class ClockPunchSchema(ma.Schema):
    class Meta:
        model = ClockPunch
        strict = True

    clock_punch_id = fields.Str(dump_only=True, attribute="clock_punch_id")
    user_id = fields.Str(required=True)
    status = fields.Str(dump_only=True)
    created_at = fields.Date(dump_only=True)

    @post_load
    def make_ClockPunch(self, data, **kwargs):
        clockPunch = ClockPunch(**data)
        return clockPunch
