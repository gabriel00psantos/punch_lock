from flask_sqlalchemy import SQLAlchemy
from passlib.hash import pbkdf2_sha256
from sqlalchemy.dialects.postgresql import UUID
import uuid
import datetime
import enum

db = SQLAlchemy()


def configure(app):
    db.init_app(app)
    app.db = db

class User(db.Model):
    __tablename__ = "user"
    #Set to String to avoid problems with sqlite db on tests
    user_id = db.Column(db.String(36), 
                                     default=lambda: str(uuid.uuid4()), 
                                     primary_key=True)
    cpf = db.Column(db.String(11), unique=True, nullable=False)
    fullname = db.Column(db.String(255), nullable=True)
    password = db.Column(db.String(255), nullable=False)
    email = db.Column(db.String(255), unique=True, nullable=False)
    created_at = db.Column(db.DateTime, default=datetime.datetime.utcnow)

    def gen_hash(self):
        self.password = pbkdf2_sha256.hash(self.password)

    def verify_password(self, password):
        return pbkdf2_sha256.verify(password, self.password)

class ClockPunchStatus(enum.Enum):
    entrada = "Entrada"
    saida = "Saida"

    def __str__(self):
        return self.value

class ClockPunch(db.Model):
    __tablename__ = "clock_punch"
    #Also Set to String to avoid problems with sqlite db on tests
    clock_punch_id = db.Column(db.String(36), 
                                     default=lambda: str(uuid.uuid4()), 
                                     primary_key=True)
    user_id = db.Column(db.String(36), db.ForeignKey('user.user_id'))
    status = db.Column(db.Enum(ClockPunchStatus))
    created_at = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    user = db.relationship('User', backref='user_clockpunches', lazy='select',
                                cascade="all, delete")

class ConcludedDay(db.Model):
    __tablename__ = "days_concluded"
    clock_counter_id = db.Column(db.String(36), 
                                     default=lambda: str(uuid.uuid4()), 
                                     primary_key=True)
    user_id = db.Column(db.String(36), db.ForeignKey('user.user_id'))
    entry = db.Column(db.String(36), db.ForeignKey('clock_punch.clock_punch_id'))
    departure = db.Column(db.String(36), db.ForeignKey('clock_punch.clock_punch_id'))
    user = db.relationship('User', backref='user_full_days', lazy='select',
                                cascade="all, delete")
    total_seconds = db.Column(db.Float(), nullable=False)
    
