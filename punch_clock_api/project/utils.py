from flask import current_app
import requests
import logging

from project.model import ConcludedDay


logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)

def conclude_day(is_departure, last_clock, payload, current_app):
    if is_departure:
        initial_hour = last_clock.created_at
        final_hour = payload.created_at
        delta = (final_hour - initial_hour)
        day_concluded = ConcludedDay(
            user_id=payload.user_id,
            entry=payload.clock_punch_id,
            departure=last_clock.clock_punch_id,
            total_seconds=delta.total_seconds()
        )
        current_app.db.session.add(day_concluded)
        current_app.db.session.commit()

def seconds_conversion(sec):
   sec_value = sec % (24 * 3600)
   hour_value = sec_value // 3600
   sec_value %= 3600
   min = sec_value // 60
   sec_value %= 60
   return hour_value
