import logging
from flask import Blueprint, request, jsonify, current_app
from datetime import timedelta
from sqlalchemy.sql import func
from flask_jwt_extended import (jwt_required, create_access_token, 
    create_refresh_token, get_jwt_identity)
from project.model import User, ConcludedDay
from project.serializer import (UserInsertUpdateSchema, LoginSchema, 
UserListSchema)

logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)

bp_user = Blueprint('user', __name__)

@bp_user.route('/login', methods=['POST'])
def login():
    user, error = LoginSchema().load(request.json)

    if error:
        return {'error': error}, 401

    user = User.query.filter_by(cpf=user.cpf).first()
    
    if user and user.verify_password(request.json['password']):
        acess_token = create_access_token(
            identity=user.user_id,
            expires_delta=timedelta(days=1)
        )

        return jsonify({
            'acess_token': acess_token,
            'message': 'Success :)'
        }), 200

    return jsonify({
        'message': 'Invalid credentials'
    }), 401

@bp_user.route("/user", methods=['GET'])
@jwt_required
def listOneAll():
    cSchema = UserListSchema(many=True)
    u_id = request.args.get('user_id')
    user_id = get_jwt_identity()
    if u_id:
        result = User.query.filter(User.user == u_id)
    else:
        result = User.query.all()

    data = cSchema.jsonify(result)
    status = 404 if not data else 200
    return data, status

@bp_user.route('/signin', methods=['POST'])
def register():
    try:
        us = UserInsertUpdateSchema()
        user, error = us.load(request.json)
        if error:
            return {'error': error}, 401

        user.gen_hash()
        current_app.db.session.add(user)
        current_app.db.session.commit()
        return us.jsonify(user), 201
    except Exception as e:
        logging.error('Error on insert user {}'.format(str(e)))
        return {'error': 'Error on insert user'}, 500

@bp_user.route('/user', methods=['PUT'])
def update():
    try:
        us = UserInsertUpdateSchema()
        user, error = us.load(request.json)
        if error:
            return {'error': error}, 401
        id = user.user_id
        query = User.query.get(id)
        user.gen_hash()
        current_app.db.session.merge(user)
        current_app.db.session.commit()
        return us.jsonify(user), 201
    except Exception as e:
        logging.error('Error on update user {}'.format(str(e)))
        return {'error': 'Error on update user'}, 500

@bp_user.route("/user", methods=['DELETE'])
@jwt_required
def delete():
    u_id = request.args.get('user_id')
    if not u_id:
        return jsonify({'error': 'Missing Field "user_id"'}), 401
    u_obj = User.query.filter(User.user_id == u_id).first()
    if not u_obj:
        return jsonify({}), 404
    else:
        current_app.db.session.delete(u_obj)
        current_app.db.session.commit()
        return jsonify({}), 204

