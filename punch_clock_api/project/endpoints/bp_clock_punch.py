import logging
import datetime

from flask import Blueprint, request, current_app, jsonify
from flask_jwt_extended import jwt_required, get_jwt_identity
from marshmallow.exceptions import ValidationError
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.sql import func

from project.serializer import ClockPunchSchema
from project.model import ClockPunch, ClockPunchStatus, ConcludedDay
from project.utils import conclude_day, seconds_conversion

bp_clock_punch = Blueprint('clock_punch', __name__)

@bp_clock_punch.route("/clock_punch/", methods=['GET'])
@jwt_required
def listAll():
    cSchema = ClockPunchSchema(many=True)
    user_id = get_jwt_identity()
    result = ClockPunch.query.filter(ClockPunch.user_id == user_id)
    total_seconds = ConcludedDay.query.with_entities(
        func.sum(ConcludedDay.total_seconds).label('total_seconds')
    ).filter(ConcludedDay.user_id == user_id).first()[0]

    c_punches = cSchema.dump(result)
    data = {}
    if total_seconds and c_punches:
        data = {
            'total_hours': seconds_conversion(total_seconds),
            'clock_punches': c_punches
        }

    status = 404 if not data else 200
    return data, status

@bp_clock_punch.route("/clock_punch/", methods=['POST'])
@jwt_required
def create():
    cSchema = ClockPunchSchema()
    user_id = get_jwt_identity()
    request.json['user_id'] = user_id
    
    last_clock = ClockPunch.query.\
        filter(ClockPunch.user_id == user_id).\
        order_by(ClockPunch.created_at.desc()).first()

    is_departure = (last_clock and\
                   (last_clock.status == ClockPunchStatus.entrada))
    
    try:
        payload, error = cSchema.load(request.json)
        if error:
            return {'error': error}, 422
        
        if is_departure:
            payload.status = ClockPunchStatus.saida
        else:
            payload.status = ClockPunchStatus.entrada
        
        current_app.db.session.add(payload)
        current_app.db.session.commit()
        
        conclude_day(is_departure, last_clock, payload, current_app)

        result = cSchema.jsonify(payload)
    except SQLAlchemyError as e:
        current_app.db.session.rollback()
        return jsonify({'error': 'Data related error'}), 500
    except Exception as e:
        return {'error': str(e)}, 500
    else:
        return result, 200
