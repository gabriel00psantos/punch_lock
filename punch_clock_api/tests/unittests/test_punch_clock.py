import time
from flask import url_for
from .flask_base_tests_cases import TestFlaskBase


class TestInsert(TestFlaskBase):
    def test_clock_punch_ok(self):
        self.create_user()
        token = self.create_token()
        dado1 = {}

        response = self.client.post(
            url_for('clock_punch.create'),
            json=dado1,
            headers=token
        )
        self.assertEqual(response.status_code, 200)


class TestShow(TestFlaskBase):
    def test_show_empty_query(self):
        self.create_user()
        token = self.create_token()
        response = self.client.get(
            url_for('clock_punch.listAll'),
            headers=token
        )
        self.assertEqual({}, response.json)
        self.assertEqual(response.status_code, 404)

    def test_show_element_inserted(self):
        self.create_user()
        token = self.create_token()
        dado1 = {}

        response = self.client.post(url_for('clock_punch.create'), headers=token,
                                    json=dado1)
        
        time.sleep(3)

        response = self.client.post(url_for('clock_punch.create'), headers=token,
                                    json=dado1)
        response = self.client.get(
            url_for('clock_punch.listAll'), headers=token)
        self.assertEqual(2, len(response.json))

