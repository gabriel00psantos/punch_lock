from flask import url_for
from .flask_base_tests_cases import TestFlaskBase


class TestUserBP(TestFlaskBase):
    def test_register_users_ok(self):
        user = {
            "cpf": "44444444444",
            "fullname": "gabrielson",
            "email": "gabriel0ps@gmail.com",
            "password": "senha123"
        }

        esperado = {
            "cpf": "44444444444",
            "email": "gabriel0ps@gmail.com",
            "fullname": "gabrielson",
        }

        response = self.client.post(url_for('user.register'), json=user)

        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.json['fullname'], esperado['fullname'])

    def test_register_users_error_validation(self):
        user = {
            "cpf": "44444444444",
            "fullname": "gabrielson",
            "email": "gabriel0ps@gmail.com"
        }


        esperado = {'error': {'password': ['Missing data for required field.']}}

        response = self.client.post(url_for('user.register'), json=user)

        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.json, esperado)

    def test_login_user_bad_credentials(self):
        user = {
            "cpf": "44444444444",
            "fullname": "gabrielson",
            "email": "gabriel0ps@gmail.com",
            "password": "senha1234"
        }
        
        esperado = {
            'id': '1',
            'fullname': 'gabrielson'
        }
        response = self.client.post(url_for('user.register'), json=user)

        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.json['fullname'], esperado['fullname'])

        wrong_pass = {'password': 'senha123'}
        user.update(wrong_pass)

        response_login = self.client.post(url_for('user.login'), json=user)
        self.assertEqual(response_login.status_code, 401)





