# punch-clock Implementation

## Instruções

Sistema simples para controle de entrada e saída de
uma empresa. O sistema deve permitir o cadastro de usuários e o registro de ponto dos
mesmos.

#### Construa o ambiente

    docker-compose up --b --d

#### Rode as migrations

      docker-compose exec web flask db init
      docker-compose exec web flask db migrate
      docker-compose exec web flask db upgrade

#### Rode os testes

	docker-compose exec web coverage run --source=project -m unittest discover -s tests/

#### Gerando relatórios de cobertura

    docker-compose exec web coverage html                                                           


#### REST API

Se tudo deu certo, a um health-check estara disponivel no endereço http://localhost:5000/health-check

#### Com tudo rodando

exportar o arquivo clock_punch.postman_collection.json para o seu postman, se cadastrar, logar e usar os endpoints.


